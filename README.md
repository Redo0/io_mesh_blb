BLB Importer for Blender
by Redo

This is a modified version of
https://forum.blockland.us/index.php?topic=244029.0
with new features, including:

* Correct importing of normals
* Correct importing of UV Coords
* Importing of color blend modes in a way that can be read by the BLB Exporter
* Detecting and importing of smooth shading

Compatible exporter: https://github.com/DemianWright/io_scene_blb
